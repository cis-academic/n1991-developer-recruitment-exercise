﻿using Microsoft.AspNetCore.Mvc;

namespace RagStatus.Api.Controllers
{
    /// <summary>
    /// This is an example of an API controller (and how to get documentation to appear on /swagger).
    /// Feel free to delete it when you create your own.
    /// Dependencies from the service container may be injected via constructor arguments.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class SumController
    {
        /// <summary>
        /// Adds two integers together.
        /// </summary>
        /// <param name="first">The first integer</param>
        /// <param name="second">The second integer</param>
        /// <returns>The sum of the two integers</returns>
        /// <response code="200">Sum calculated</response>
        /// <response code="400">Invalid inputs</response>
        [HttpPost]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(400)]
        public IActionResult GetSum(int first, int second) => 
            new OkObjectResult(first + second);
    }
}
