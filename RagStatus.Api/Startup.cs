﻿using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using RagStatus.Service;

namespace RagStatus.Api
{
    /// <summary>
    /// Defines service container and HTTP request pipeline for the RAG Status API.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Create a Startup from the supplied configuration.
        /// </summary>
        /// <param name="configuration">The application configuration.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        
        /// <summary>
        /// The application configuration.
        /// </summary>
        public IConfiguration Configuration { get; }


        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">The dependency injection container configuration.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddControllers();
            services.AddSwaggerGen(_ =>
            {
                _.SwaggerDoc("v1", new OpenApiInfo { Title = "RAG Status API", Version = "v1" });
                _.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "RagStatus.Api.xml"));
            });

            // Dependency injection configuration.
            services.AddSingleton<IRagCalculator, RagCalculator>();
        }


        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The application builder.</param>
        /// <param name="env">The web host environment.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app
                .UseRouting()
                .UseEndpoints(_ => {
                    _.MapControllers();
                })
                .UseSwagger()
                .UseSwaggerUI(_ => {
                    _.SwaggerEndpoint("/swagger/v1/swagger.json", "RAG Status API V1");
                });
        }
    }
}
