﻿using RagStatus.Service;
using System;

namespace RagStatus.CommandLineTool
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "RAG Status Service";
            var ragCalculator = new RagCalculator();

            do
            {
                Console.Write("Enter the percentage of lectures missed: ");
                if (!int.TryParse(Console.ReadLine(), out int missedLecturePercentage))
                    break;

                Console.Write("Enter the number of coursework submissions missed: ");
                if (!int.TryParse(Console.ReadLine(), out int missedCourseworkSubmissions))
                    break;

                var ragStatus = ragCalculator.GetRagStatus(missedLecturePercentage, missedCourseworkSubmissions);
                Console.WriteLine($"The student's RAG status is: {ragStatus}");

                Console.WriteLine("Another?");
            }
            while (Console.ReadKey(true).Key == ConsoleKey.Y);
        }
    }
}
