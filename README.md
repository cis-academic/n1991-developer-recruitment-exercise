## Introduction

Student retention is an important focus for the University. Poor lecture attendance and coursework submission rates are leading indicators that a student is at greater risk of withdrawing from their course. Early intervention can help students to remain on track to graduate.

This assignment is to develop a service which can be used to assess when it is appropriate for students' departments to intervene. We use a RAG (red, amber, green) indicator to highlight which students are most at risk.

The thresholds are:

>**Red**: The student has missed at least 30% of lectures, or has failed to submit any piece of coursework;  
>**Amber**: The student has missed at least 15% of lectures and less than 30% of lectures;  
>**Green**: Otherwise

## Assignment Instructions

- As a guide, this assignment should take you around 2-4 hours to complete.
- Clone this repository to get started. *Please don't publish your solution publicly.* We want to make sure everybody's work is their own.
- We have provided this starter solution, including Nuget dependencies on Swashbuckle, xunit, NSubstitute and Shouldly, which are our preferred documentation, unit testing, mocking and assertion frameworks. But feel free to choose your own.
- Tackle the stories in order.
- All stories should be completed with an appropriate level of testing.
- You should aim to produce SOLID and DRY code.
- Commit at least after each story with meaningful commit messages.
- We will be assessing your submission for code quality, attention to detail and approach.

## Submission Instructions

- Create a free account at bitbucket.org if you don't already have one.
- Create a new *private* repository and push your solution to it.
- Give cis-academic read access to your repository (Settings -> User and group access)
- Email liz@lancaster.ac.uk once you have done this.
- The deadline for submissions is 9am on Monday 25th November 2019.

## Stories

### Story 1

>As a **director of studies**  
>I want to **calculate the RAG status for a student**  
>So that **I can determine when to meet with a student**

_Acceptance Criteria_

- The command line tool outputs the RAG status calculated correctly according to the thresholds set out above
- You can assume that the RAG metrics (attendance and missed submissions) will be input into the calculator rather than loaded from a store
- Invalid inputs are dealt with appropriately


### Story 2

>As a **service manager**  
>I want to **be able to configure the attendance thresholds**  
>So that **I can change the thresholds without changing code**

_Acceptance Criteria_

- The command line tool can be used to adjust the percentage of lectures missed for both the red and amber statuses
- For this exercise, you can stub out the configuration store instead of actually persisting the configuration

### Story 3

>As a **system integrator**  
>I want **an API for the RAG service**  
>So that **I can build workflows that react to RAG status changes**

_Acceptance Criteria_

- The API should be RESTful (i.e. it should make appropriate use of HTTP verbs, resources, query parameters, content negotiation and status codes).
- It should respond appropriately to invalid requests.
- It should have an appropriate amount of documentation.

### Story 4

>As a **director of studies**  
>I want to **include virtual learning environment (VLE) activity in the RAG status**  
>So that **I can better identify students who have become disengaged**

_Acceptance Criteria_

- The API and command line tool must be modified to report Amber status when:
  >**Amber**: The student has missed at least 15% of lectures and less than 30% of lectures, or last engaged with the VLE more than 14 days ago;
- The API changes must be backwards compatible with the previous version

## Getting Started

This section is intended to help developers who are not familiar with the tools and frameworks we use to get a development environment set up and get started. If you have any questions, please [ask](mailto:t.chart@lancaster.ac.uk).

### Knowledge

The assignment is to produce some C# code. This doesn't require deep knowledge of the language features or the .NET ecosystem. Some familiarity with object-oriented programming concepts and either [C# syntax](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/interfaces/) or Java syntax will suffice.

### Tools

You will need:
- [git](https://git-scm.com/downloads)
- The [.NET Core 3.0 SDK](https://dotnet.microsoft.com/download)
- An IDE (optional). We recommend [Visual Studio 2019 Community Edition](https://visualstudio.microsoft.com/downloads/) (Windows) or [Visual Studio Code](https://code.visualstudio.com/download) (Windows/Linux/Mac). Both are free and sufficient.

### Source Control

To clone this starter solution, press the 'Clone' button at the top of BitBucket page to get the clone command (`git clone...`). Once you've cloned the repository to your development machine, create a new private git repository in BitBucket to host your solution, and follow the instructions BitBucket gives you to push the starter solution to it (`git remote set-url...` then `git push...`).

As you make changes to the code in your local repository,
- `git status` will tell you what files have changed,
- `git diff` will list your changes, and
- `git add *`, then `git commit -m "<your commit message>"`, then `git push` will commit all your local changes and push them to your BitBucket repository.

Remember to share the repository with us when you are done!

### .NET CLI

You can use the `dotnet` command line interface from the cross-platform .NET Core SDK as follows:
  - Build: `dotnet restore` then `dotnet build`
  - Run the tests: `dotnet test`
  - Run the command-line tool: `dotnet run --project RagStatus.CommandLineTool`.
  - Run the API: `dotnet run --project RagStatus.Api`, then direct your browser to `/swagger` under the URL the API is listening on.