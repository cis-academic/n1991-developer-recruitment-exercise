﻿namespace RagStatus.Service
{
    public interface IRagCalculator
    {
        /// <summary>
        /// Determines the RAG status of a student on the basis of some input metrics.
        /// </summary>
        /// <param name="missedLecturePercentage">The percentage of lectures the student has failed to attend.</param>
        /// <param name="missedCourseworkSubmissions">The number of coursework assessments the student has failed to submit.</param>
        /// <returns>The RAG status of the student, which will be "red", "amber" or "green".</returns>
        string GetRagStatus(int missedLecturePercentage, int missedCourseworkSubmissions);
    }
}
