using RagStatus.Service;
using Shouldly;
using Xunit;

namespace RagStatus.Tests
{
    public class RagCalculatorTests
    {
        // Arrange
        protected IRagCalculator Calculator { get; } = new RagCalculator();

        public class GetRagStatusTests : RagCalculatorTests
        {
            [Fact]
            public void Should_return_green_when_the_student_has_no_missed_lectures_or_courseworks()
            {
                // Act
                var ragStatus = Calculator.GetRagStatus(0, 0);

                // Assess
                ragStatus.ShouldBe("green");
            }
        }
    }
}
